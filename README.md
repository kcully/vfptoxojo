# README #

The VFPToXojo project is meant to be complimentary to the Xojo_VacumeControls This application is built in VFP9.  The binary VFP projects, screens have been converted to source control format using the FoxBin2PRG program from http://vfpx.codeplex.com.

### What is this repository for? ###

* Quick summary: The VFPToXojo project is meant to output a valid Xojo project from what (little) it can convert from VFP Projects, forms, and programs.  The VFPToXojo project uses the template data held in data\tcontroltemplate.dbf to convert properties, methods, and events into Xojo format.  This is a very ALPHA product. The likelihood that this application will convert a heavily OOP VFP application is miniscule. This was more of a proof-of-concept attempt at a VFP application converter.  Xojo accesses data very differntly than VFP and that alone will make converting VFP applications difficult.
* Version: Alpha 0.1

### How do I get set up? ###

* Copy/download/pull all files to a VFP friendly directory. Example: c:\vfp\vfptoxojo; c:\vfp\vfptoxojo\data; c:\vfp\vfptoxojo\form; c:\vfp\vfptoxojo\prg
* Run the "make_table_tcontroltemplate.prg" from within VFP9.  This should create data\tcontroltemplate.dbf and data\tcontroltemplate.cdx.
* NOTE: If you've updated this table with a more modern version of the Xojo template using the Xojo_VacumeControls application, use that data by copying the data from that directory into the data directory of this application.
* Run the FoxBin2PRG program to convert the vfptoxojo.pj2 back to a PJX project set, plus the form\vfptoxojo.sc2 back to a SCX form set, plus the form\vfpcodeconverter.sc2 back to the SCX form set.
* Build the vfptoxojo project into an EXE
* Run the program and point it to a current VFP project, adjust your settings, and click the [Convert] button.
* If all is successful, a valid Xojo project will be in the OUTPUT directory.

### Contribution guidelines ###

* This application was really created as a "proof of concept" application. It is not really meant to convert complex VFP projects to Xojo.
* If you're really passionate about the application, please test thoroughly and then push changes back to the repository.

### Who do I talk to? ###

* Kevin Cully is the Repo owner but don't contact him for support.  Roll up your sleeves, get your debugging hat on, push forward, and have fun.