SET EXACT OFF

SET PROCEDURE TO ctSettings ADDITIVE

PUBLIC goConverter
goConverter = CREATEOBJECT( "VFPToXojoConverter" )

DO FORM VFPToXojo

&& CLOSE DATABASES ALL


*=========================================================
*=========================================================
*=========================================================
*=========================================================
DEFINE CLASS VFPToXojoConverter AS Custom

	cBeginTag 		= [{{!}]
	cEndTag 		= [{/!}}]
	cTempCursor 	= []
	cControlsMethods= [c_ControlMethods]
	cFormMethods	= [c_FormMethods]
	CRLF			= []
	cVersion 		= [0.4 Beta]
	lTargetWeb		= .F.
	lCommentOutCode = .F.


*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 10/07/13 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION Init()
	THIS.CRLF = CHR(13) + CHR(10)
	DODEFAULT()
ENDFUNC

*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 05/03/11 
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*   type="H", "Project", type="K", "Form   ", type="P", "Program", type="R", "Report ", "Unknown"
*!*		KJC 03/06/2012 - Added all form references are now lower case.
*!*		KJC 03/06/2012 - Added to the OutputControl call not to include any spaces.
*!*		KJC 03/06/2012 - Setting the AppIcon value.  Project complains otherwise.
FUNCTION ConvertProject( toProject AS Object )
	LOCAL llRetVal, lcOrigPath, lcProjectName, lcProjectPath, lcForms, lcModules, lcFirstForm, llNoSpaces
	LOCAL lcBaseFileName, lcTarget, lcProperties
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	llRetVal = .F.
	lcForms = []
	
	=OpenDBF("data\tControlTemplate")
	WAIT WINDOW toProject.Name NOWAIT
	=CloseDBF("tmpProject")
	
	lcOrigPath = SYS(5) + CURDIR()
	
	lcProjectName = LOWER( toProject.Name )
	
	USE ( lcProjectName ) IN 0 ALIAS tmpProject AGAIN
	
	lcProjectPath = JUSTPATH( toProject.Name )
	
	SET DEFAULT TO (lcProjectPath)
	lcForms 	= THIS.ConvertProjectForms()
	lcFirstForm = GETWORDNUM( lcForms, 1, ';' )
	lcModules	= THIS.ConvertProjectModules()

*!*		SELECT LOWER(Key) AS cKey, ID FROM tmpProject WHERE Type = "K" INTO CURSOR c_Forms NOFILTER
*!*		SELECT c_Forms
*!*		lcFirstForm = ALLTRIM(c_Forms.cKey)
*!*		SCAN
*!*			lcWindowHex = TRANSFORM( c_Forms.ID, "@0" )
*!*			lcWindowHex = SUBSTR( lcWindowHex, 2 )
*!*			lcForms = lcForms + IIF(EMPTY(lcForms), [], CHR(13)+CHR(10)+"Window=") + ;
*!*				ALLTRIM(c_Forms.cKey) + ";" + ALLTRIM(c_Forms.cKey)+".xojo_window;&h" + lcWindowHex + ";&h0;false"
*!*		ENDSCAN

	lcTargetType = IIF( THIS.lTargetWeb, "web", "desktop" )
	lcWindowTag = IIF( THIS.lTargetWeb, "WebView", "Window" )

	DO CASE
		CASE NOT THIS.GetControlTemplate( "project_" + lcTargetType, lcTarget )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "Type", lcTargetType, THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "Class", "App;App.xojo_code;&h853EFFF;&h0;false" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, lcWindowTag, lcForms )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "Module", lcModules )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "MenuBar", "", THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "Report", "", THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "DefaultWindow", lcFirstForm )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "AppMenuBar", "", THIS.lTargetWeb )
		&&CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "AppIcon", JUSTSTEM( lcProjectName ) + ".rbres;&h0" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "MajorVersion", "" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "WindowsName", FORCEEXT( JUSTFNAME(lcProjectName), "exe") )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "MacCarbonMachName", JUSTSTEM( lcProjectName ) )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "LinuxX86Name", JUSTSTEM(lcProjectName) )
		OTHERWISE
			SET DEFAULT TO (lcOrigPath)
			llNoSpaces = .T.
			lcRetVal = THIS.OutputControl( lcTarget, 0, llNoSpaces )
			=STRTOFILE( lcRetVal, "output\" + FORCEEXT( JUSTFNAME( lcProjectName ), 'xojo_project'), 0 )
			llRetVal = .T.
			
			&& SET STEP ON 
	ENDCASE
	
	THIS.ConvertBuildAutomation()
		
	=CloseDBF("c_Forms")
	=CloseDBF("tmpProject")
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	WAIT CLEAR
	
	
RETURN llRetVal


*!* ====================================
*!* Program: ConvertProjectForms
*!* Author: Kevin Cully
*!* Date: 10/14/2013
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertProjectForms( )
	LOCAL lcForms, lnOrigArea
	lcForms = []
	lnOrigArea = SELECT()

	SELECT LOWER(Key) AS cKey, ID FROM tmpProject WHERE Type = "K" INTO CURSOR c_Forms NOFILTER
	SELECT c_Forms
	SCAN
		lcWindowHex = TRANSFORM( c_Forms.ID, "@0" )
		lcWindowHex = SUBSTR( lcWindowHex, 2 )
		lcForms = lcForms + IIF(EMPTY(lcForms), [], CHR(13)+CHR(10)+"Window=") + ;
			ALLTRIM(c_Forms.cKey) + ";" + ALLTRIM(c_Forms.cKey)+".xojo_window;&h" + lcWindowHex + ";&h0;false"
	ENDSCAN
	=CloseDBF("c_Forms")
	SELECT (lnOrigArea)
	
RETURN lcForms


*!* ====================================
*!* Program: ConvertProjectForms
*!* Author: Kevin Cully
*!* Date: 10/14/2013
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertProjectModules( )
	LOCAL lcModules, lnOrigArea
	lcModules = []
	lnOrigArea = SELECT()

	SELECT LOWER(Key) AS cKey, ID FROM tmpProject WHERE Type = "P" INTO CURSOR c_Modules NOFILTER
	SELECT c_Modules
	SCAN
		lcWindowHex = TRANSFORM( c_Modules.ID, "@0" )
		lcWindowHex = SUBSTR( lcWindowHex, 2 )
		lcModules = lcModules + IIF(EMPTY(lcModules), [], CHR(13)+CHR(10)+"Module=") + ;
			ALLTRIM(c_Modules.cKey) + ";" + ALLTRIM(c_Modules.cKey)+".xojo_code;&h" + lcWindowHex + ";&h0;false"
	ENDSCAN
	=CloseDBF("c_Modules")
	SELECT (lnOrigArea)
	
RETURN lcModules


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/28/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertApp( toProjThing AS Object )
	LOCAL llRetVal
	LOCAL lcBaseFileName, lcTarget
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [App] + lcBaseFileName
	llRetVal = .F.
	DO CASE
		CASE NOT THIS.GetControlTemplate( "app", lcTarget )
			SET STEP ON 
		OTHERWISE
			lcRetVal = THIS.OutputControl( lcTarget, 0 )
			=STRTOFILE( lcRetVal, "output\app.xojo_code", 0 )
			llRetVal = .T.
			
			&& SET STEP ON 
	ENDCASE
	=CloseDBF(lcTarget)
RETURN llRetVal
ENDFUNC

*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 05/03/11 
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 03/06/2012 - Fixed the output form file name to be based on toProjThing.Name.
FUNCTION ConvertForm( toProjThing AS Object )
	LOCAL llRetVal, lcAlias, lcFullPath, llContinue, loForm, lcOutput
	llRetVal = .F.
	&& lcAlias = "frm" + JUSTSTEM( ALLTRIM(toProjThing.Name ) )
	lcFullPath = FULLPATH( ALLTRIM( toProjThing.Name ) )
	llContinue = FILE( lcFullPath )
	IF NOT llContinue
		lcFullPath = ADDBS( toProjThing.cHomeDir ) + ALLTRIM( toProjThing.Name )
		llContinue = FILE( lcFullPath )
	ENDIF
	
	IF llContinue
		SELECT 0
		IF OpenDBF( lcFullPath)
			THIS.cTempCursor = ALIAS()
			WAIT WINDOW toProjThing.Name NOWAIT
			LOCATE FOR BaseClass = "form"
			IF FOUND()
				SCATTER NAME loForm MEMO
				
				lSuccess = THIS.ConvertForm_Methods( loForm )
				
				lcOutput = THIS.ConvertControlForm( loForm, 0 )
				lcOutput = lcOutput + THIS.ConvertFormChild( loForm.ObjName, loForm.UniqueID, 1 )
				lcOutput = lcOutput + THIS.ConvertControlFormClose( loForm, 0 )
				
				lcOutput = lcOutput + THIS.OutputMethods( THIS.cFormMethods )
				lcOutput = lcOutput + THIS.OutputMethods( THIS.cControlsMethods )
				lcOutExt = IIF( THIS.lTargetWeb, "xojo_code", "xojo_window" )
				=STRTOFILE( lcOutput, "output\" + FORCEEXT( JUSTFNAME( toProjThing.Name), lcOutExt), 0 )
			ENDIF			
		ENDIF
	ENDIF
	=CloseDBF( JUSTSTEM( lcFullPath ) )
	=CloseDBF( THIS.cFormMethods )
	=CloseDBF( THIS.cControlsMethods )
	
RETURN llRetVal

*!* ====================================
*!* Program: Convert Form Child
*!* Author: Kevin Cully
*!* Date: 02/02/12 
*!* Copyright:
*!* Description:
*!*		This method handles the recursion through the form hierarchy.
*!* Revision Information:
FUNCTION ConvertFormChild( tcObjName, tcUniqueID, tnLevel )
	LOCAL lcRetVal, lcAlias
	lcRetVal = THIS.ConvertControl( tcUniqueID, tnLevel )
	
	lcAlias = "c_L" + TRANSFORM( tnLevel ) + SYS(2015)
	SELECT CAST( ObjName AS C(50)) AS cObjName, UniqueID ;
		FROM (THIS.cTempCursor) ;
		WHERE NOT EMPTY(Parent) ;
			AND Parent == tcObjName INTO CURSOR (lcAlias)
	SELECT (lcAlias)
	SCAN
		SCATTER NAME loChild
		lcRetVal = lcRetVal + THIS.ConvertFormChild( tcObjName + "." + loChild.cObjName, loChild.UniqueID, tnLevel + 1 )
	ENDSCAN
	 
	=CloseDBF(lcAlias)
		
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program: ConvertBuildAutomation
*!* Author: Kevin Cully
*!* Date: 10/14/2013
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertBuildAutomation()
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, llNoSpaces
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcTemplate		= "BuildAutomation"	
	lnLevel			= 1
	llNoSpace		= .F.
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		OTHERWISE
			llNoSpaces = .F.
			lcRetVal = THIS.OutputControl( lcTarget, lnLevel, llNoSpaces )
			&& SET STEP ON 
			=STRTOFILE( lcRetVal, "output\Build Automation.xojo_code", 0 )
			llRetVal = .T.
			
	ENDCASE
	=CloseDBF( lcTarget )
	
RETURN lcRetVal
ENDFUNC



*!* ====================================
*!* Program: ConvertControl
*!* Author: Kevin Cully
*!* Date: 02/02/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControl( tcUniqueID, tnLevel )
	LOCAL lcRetVal, lcMessage, lnOrigArea
	lcRetVal = []
	lcMessage = []
	lnOrigArea = SELECT()
	IF USED( THIS.cTempCursor )
		SELECT (THIS.cTempCursor)
		LOCATE FOR UniqueID == tcUniqueID
		IF FOUND()
			SCATTER NAME loControl MEMO
			loControl.BaseClass = ALLTRIM( loControl.BaseClass )
			lcMessage = TRANSFORM(tnLevel) + REPLICATE(" . ", tnLevel) + loControl.ObjName + " which is a " + loControl.BaseClass
			&& ? lcRetVal
			DEBUGOUT lcMessage
			WAIT WINDOW lcMessage TIMEOUT 0.1
			DO CASE
				CASE loControl.BaseClass = "pageframe"
					lcRetVal = THIS.ConvertControlPageFrame( loControl, tnLevel )
				CASE loControl.BaseClass = "checkbox"
					lcRetVal = THIS.ConvertControlCheckBox( loControl, tnLevel )
				CASE loControl.BaseClass = "combobox"
					lcRetVal = THIS.ConvertControlComboBox( loControl, tnLevel )
				CASE loControl.BaseClass = "commandbutton"
					lcRetVal = THIS.ConvertControlCommandButton( loControl, tnLevel )
				CASE loControl.BaseClass = "grid"
					lcRetVal = THIS.ConvertControlGrid( loControl, tnLevel )
				CASE loControl.BaseClass = "image"
					lcRetVal = THIS.ConvertControlImage( loControl, tnLevel )
				CASE loControl.BaseClass = "label"
					lcRetVal = THIS.ConvertControlLabel( loControl, tnLevel )
				CASE loControl.BaseClass = "listbox"
					lcRetVal = THIS.ConvertControlListbox( loControl, tnLevel )
				CASE loControl.BaseClass = "optiongroup"
					lcRetVal = THIS.ConvertControlOptionGroup( loControl, tnLevel )
				CASE loControl.BaseClass = "textbox"
					lcRetVal = THIS.ConvertControlTextbox( loControl, tnLevel )
				CASE loControl.BaseClass = "editbox"
					lcRetVal = THIS.ConvertControlEditbox( loControl, tnLevel )
				CASE loControl.BaseClass = "form"
					lcMessage = "Baseclass is form.  Not converting as this is where we are starting anyway."
					WAIT WINDOW lcMessage TIMEOUT 0.1
				OTHERWISE
					lcMessage = "Unsupported control type: " + loControl.BaseClass
					&& WAIT WINDOW lcMessage TIMEOUT 1
					WAIT WINDOW lcMessage NOWAIT
					DEBUGOUT lcMessage
					&& SET STEP ON 
			ENDCASE
		ELSE
			&& If we're converting a pageframe, then there isn't a UniqueID for this page.
			&& This is possibly valid.
			&& SET STEP ON 
		ENDIF
	ELSE
		SET STEP ON 
	ENDIF
	
	SELECT (lnOrigArea)
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 03/06/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlOptionGroup( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcButtonCount, lnButtonCount, ;
		lnOptTop, lnOptLeft, lnX, lcRadioButton, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [optr] + lcBaseFileName
	lcProperties 	= [optv] + lcBaseFileName
	lcTemplate		= IIF( THIS.lTargetWeb, "webradiogroup", "radiobutton" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
			SET STEP ON 
		OTHERWISE
			lcButtonCount = THIS.GetVFPValue(lcProperties, "ButtonCount")
			lnButtonCount = INT(VAL(lcButtonCount))
			lnOptTop = INT( VAL( THIS.GetVFPValue( lcProperties, "Top" ) ) )
			lnOptLeft = INT( VAL( THIS.GetVFPValue( lcProperties, "Left" ) ) )
			
			FOR lnX = 1 TO lnButtonCount
				lcControlName = "Option" + ALLTRIM( STR( lnX ) )
				=CloseDBF( lcTarget )

				THIS.GetControlTemplate( lcTemplate, lcTarget )
				&& Set optional properties ...
				llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
				llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Caption", lcControlname + ".Caption", THIS.lTargetWeb )
				llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", lcControlName + ".Height" )
				llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "Index", TRANSFORM(lnX) )
				llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "InitialParent", "TabPanel" + TRANSFORM(lnX), THIS.lTargetweb )
				llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", lcControlName + ".Left", lnOptLeft )
				llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "TabPanelIndex", TRANSFORM( tnLevel - 1), THIS.lTargetWeb )
				llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", lcControlName + ".Top", lnOptTop )
				llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", lcControlName + ".Width" )
				lcRadioButton = THIS.OutputControl( lcTarget, tnLevel )
				lcRetVal = lcRetVal + lcRadioButton
			ENDFOR
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/03/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 02/28/2012 - Passing the page # instead of the tnLevel because we need the controls
*!*			on the page to be set onto the Tabpanel Index in Real Studio.
*!*		KJC 03/05/2012 - Changing the Target and Properties cursor name to indicate 'pgf' to
*!*			make debugging easier.
FUNCTION ConvertControlPageFrame( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcPageCount, lnPageCount, lcTabDefinition
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [pgfr] + lcBaseFileName
	lcProperties 	= [pgfv] + lcBaseFileName
	lcTabDefinition = []
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( "tabpanel", lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin TabPanel", "Name" )
		&& CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Caption", "Caption" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
		OTHERWISE
			lcTabDefinition = THIS.ConvertPageFrameTabDefinition( lcProperties )
			llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "TabDefinition", lcTabDefinition )
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
	&& Process every control that belongs to this tabpanel ...
	lcPageCount = THIS.GetPropertySetting( toControl.Properties, "PageCount")
	lnPageCount = INT(VAL( lcPageCount ))
	FOR lnX = 1 TO lnPageCount
		lcObjectName = toControl.Parent + "." + toControl.ObjName + "." + "Page" + TRANSFORM(lnX)
		lcUniqueID = "" 	&& We purposely aren't passing the UniqueID because for pageframes, there isn't a record per page, ;
							&& but the controls belong to this 'virtual' parent object.
		lcRetVal = lcRetVal + ;
			THIS.ConvertFormChild( lcObjectName, lcUniqueID, lnX )
	ENDFOR
	
	&& Now get the tab panel end valeus ...
	DO CASE
		CASE NOT THIS.GetControlTemplate( "tabpanel_end", lcTarget )
			SET STEP ON 
		OTHERWISE
			lcRetVal = lcRetVal + THIS.OutputControl( lcTarget, tnLevel )
	ENDCASE
	=CloseDBF( lcTarget )

RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/23/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertPageFrameTabDefinition( tcPropertyCursor )
	LOCAL lcRetVal
	lcRetVal = []
	
	DO CASE
		CASE NOT USED( tcPropertyCursor )
		OTHERWISE
			SELECT (tcPropertyCursor)
			SCAN
				SCATTER NAME loProp
				IF "Page" $ loProp.cProperty AND "Caption" $ loProp.cProperty
					lcRetVal = lcRetVal + ;
						IIF( EMPTY( lcRetVal ), [], [\r] ) + ;
						ALLTRIM( STRTRAN( loProp.cValue, [ ], []) )
				ENDIF
			ENDSCAN
	ENDCASE
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/03/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlCheckBox ( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "webcheckbox", "checkbox" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Caption", "Caption" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
		OTHERWISE
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/27/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlComboBox ( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "weblistbox", "listbox" )

	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
		OTHERWISE
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/03/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlCommandbutton( toControl, tnTabPanelIndex )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, llSuccess, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [cmdr] + lcBaseFileName
	lcProperties 	= [cmdv] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "webbutton", "pushbutton" )
	
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin " + lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "TabpanelIndex", TRANSFORM( tnTabPanelIndex-1 ), THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
			SET STEP ON 
		OTHERWISE
			llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Caption", "Caption" )
			lcRetVal = THIS.OutputControl( lcTarget, tnTabPanelIndex )
			
			llSuccess = THIS.ConvertControl_Methods( toControl )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
	IF EMPTY(lcRetVal)
		SET STEP ON 
	ENDIF
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/27/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 03/05/2012 - Move HEIGHT, LEFT to the optional properties.
FUNCTION ConvertControlLabel( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [lblr] + lcBaseFileName
	lcProperties 	= [lblv] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "weblabel", "label" )

	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin " + lcTemplate, "Name" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Text", "Caption" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "TabpanelIndex", TRANSFORM( tnLevel-1 ), THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
			SET STEP ON 
		OTHERWISE
			&& Set optional properties ...
			llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
			llSuccess = THIS.UpdateBackStyleProperty( lcTarget, lcProperties )
			llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/28/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlGrid( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcColumnHeadings, lcTemplate
	lcRetVal = []
	lcColumnHeadings = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "weblistbox", "listbox" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin " + lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "ColumnsResizable", "True", THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "GridLinesHorizontal", "3", THIS.lTargetWeb )		&& Thin solid line.
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "GridLinesVertical", "3", THIS.lTargetWeb )		&& Thin solid line.
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "ScrollbarHorizontal", "True", THIS.lTargetWeb)
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "ScrollbarVertical", "True", THIS.lTargetWeb)
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "TabpanelIndex", TRANSFORM( tnLevel-1 ), THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
		OTHERWISE
			SELECT (lcProperties)
			SCAN FOR "column"$LOWER(cProperty) AND "name"$LOWER(cProperty)
				lcColumnHeadings = lcColumnHeadings + IIF(EMPTY(lcColumnHeadings),[], CHR(9)) + ALLTRIM(cValue)
			ENDSCAN
			IF NOT EMPTY(lcColumnHeadings)
				llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "HasHeading", "True" )
				llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "InitialValue", lcColumnHeadings )
				IF NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "ColumnCount", "ColumnCount" )
					llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "ColumnCount", "1" )
				ENDIF
			ENDIF

			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program: ConvertControlImage
*!* Author: Kevin Cully
*!* Date: 03/13/12 
*!* Copyright:
*!* Description:
*!* Todo: Need to create the AddXojoPicture() method that registers the VFP picture with the project,
*!*		and then include the reference to the BackDrop image in the form.
*!* Revision Information:
FUNCTION ConvertControlImage( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lSuccess, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [imgr] + lcBaseFileName
	lcProperties 	= [imgv] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "webcanvas", "canvas" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )

		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		OTHERWISE
			&& lSuccess = THIS.AddXojoPicture( lcTarget, lcProperties, "Backdrop", "Picture")
			lSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			lSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC





*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/27/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		2/27/2012 - Column count is optional.
FUNCTION ConvertControlListBox ( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "weblistbox", "listbox" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( "listbox", lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		CASE NOT THIS.UpdateXojoPropertyValue( lcTarget, "TabpanelIndex", TRANSFORM( tnLevel-1 ), THIS.lTargetWeb )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
		OTHERWISE
			IF NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "ColumnCount", "ColumnCount" )
				llSuccess = THIS.UpdateXojoPropertyValue( lcTarget, "ColumnCount", "1" )
			ENDIF
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC



*!* ====================================
*!* Program: ConvertControlTextbox
*!* Author: Kevin Cully
*!* Date: 02/27/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlTextbox( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "webtextfield", "textfield" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
			SET STEP ON 
		OTHERWISE
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program: ConvertControl_Methods
*!* Author: Kevin Cully
*!* Date: 03/13/12 10:33:17 AM
*!* Copyright:
*!* Description:
*!*		Takes the controls methods and moves them into a cursor, mapping over the supported
*!*		events between VFP and Xojo.  Right now, all of the code converted over are commented
*!*		out.  Later we can convert the VFP code to Xojo code as best we can.
*!* Revision Information:
FUNCTION ConvertControl_Methods( toControl As Object )
	LOCAL llRetVal, lcTempCursor, lnLines, lcObjName, lcLine, lc2ndWord, lc2Tabs
	llRetVal = .F.
	DO CASE
		CASE ISNULL( toControl )
		CASE VARTYPE( toControl ) # "O"
		CASE EMPTY( toControl.Methods )
		CASE NOT THIS.CreateMethodsCursor( THIS.cControlsMethods)	&& Used to store all of the methods for the controls on this form.
		OTHERWISE
			WAIT WINDOW "Converting method code ..." NOWAIT
			lc2Tabs = "		"
			lc1Method = THIS.ConvertVFPCodeToXojoCode( toControl.Methods )
			lcTempCursor = "mthd" + SYS(2015)
			lnLines = THIS.SnippetIntoCursor( lc1Method, lcTempCursor )
			IF lnLines > 0
				lcObjName = ALLTRIM( toControl.ObjName)
				INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "", "")	&& Insert a blank line.
				INSERT INTO (THIS.cControlsMethods) (cControlName, cLine, memoLine) VALUES (lcObjName, "#tag Events " + lcObjName, "#tag Events " + lcObjName )
				SELECT (lcTempCursor)
				SCAN
					SCATTER NAME loCode MEMO
					lcLine = GETWORDNUM( loCode.cCode, 1, " " )
					lc2ndWord = GETWORDNUM( loCode.cCode, 2, " ")
					DO CASE
						CASE lcLine = "PROCEDURE" AND lc2ndWord = "Click"
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "	#tag Event", "	#tag Event" )
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, lc2Tabs + "Sub Action()", lc2Tabs + "Sub Action()" )
						CASE lcLine = "PROCEDURE" AND lc2ndWord = "Init"
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "	#tag Event", "	#tag Event" )
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "		Sub Open()", "		Sub Open()" )
						CASE lcLine = "PROCEDURE"		&& Now we handle all of the other events we haven't mapped yet.  I hope this doesn't break something.
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "	#tag Event", "	#tag Event" )
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "		Sub " + lc2ndWord + "()", "		Sub " + lc2ndWord + "()" )
						CASE lcLine = "ENDPROC"
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "		End Sub", "		End Sub" )
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "	#tag EndEvent", "	#tag EndEvent")
						OTHERWISE
							INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, lc2Tabs + loCode.cCode, lc2Tabs + loCode.cCode )
					ENDCASE
				ENDSCAN
				INSERT INTO (THIS.cControlsMethods) VALUES (lcObjName, "#tag EndEvents", "#tag EndEvents" )
				
			ENDIF
			
			=CloseDBF(lcTempCursor)
			WAIT CLEAR
	ENDCASE
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program: ConvertForm_Methods
*!* Author: Kevin Cully
*!* Date: 03/15/12 10:33:17 AM
*!* Copyright:
*!* Description:
*!*		Takes the form methods and moves them into a cursor, mapping over the supported
*!*		events between VFP and Xojo.  Right now, all of the code converted over are commented
*!*		out.  Later we can convert the VFP code to Xojo code as best we can.
*!* Revision Information:
FUNCTION ConvertForm_Methods( toControl As Object )
	LOCAL llRetVal, lcTempCursor, lnLines, lcObjName, lcLine, lc2ndWord, lc2Tabs, lc1Method
	llRetVal = .F.
	DO CASE
		CASE ISNULL( toControl )
		CASE VARTYPE( toControl ) # "O"
		CASE NOT THIS.CreateMethodsCursor( THIS.cFormMethods)	&& Used to store all of the methods for the controls on this form.
		OTHERWISE
			lcObjName = ALLTRIM( toControl.ObjName)
			INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "", "")	&& Insert a blank line.
			INSERT INTO (THIS.cFormMethods) (cControlName, cLine, memoLine) VALUES (lcObjName, "#tag WindowCode", "#tag WindowCode" )
			
			IF NOT EMPTY( toControl.Methods )
				WAIT WINDOW "Converting form code ..." NOWAIT
				lc2Tabs = "		"
				lcTempCursor = "frm_mtd" + SYS(2015)
				lc1Method = THIS.ConvertVFPCodeToXojoCode( toControl.Methods )
				lnLines = THIS.SnippetIntoCursor( lc1Method, lcTempCursor )

				SELECT (lcTempCursor)
				SCAN
					SCATTER NAME loCode MEMO
					lcLine = GETWORDNUM( loCode.cCode, 1, " " )
					lc2ndWord = GETWORDNUM( loCode.cCode, 2, " ")
					DO CASE
						CASE lcLine = "PROCEDURE" AND lc2ndWord = "Click"
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "	#tag Event", "	#tag Event" )
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, lc2Tabs + "Sub Action()", lc2Tabs + "Sub Action()" )
						CASE lcLine = "PROCEDURE" AND lc2ndWord = "Init"
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "	#tag Event", "	#tag Event" )
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "		Sub Open()", "		Sub Open()" )
						CASE lcLine = "PROCEDURE"		&& Now we handle all of the other events we haven't mapped yet.  I hope this doesn't break something.
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "	#tag Event", "	#tag Event" )
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "		Sub " + lc2ndWord + "()", "		Sub " + lc2ndWord + "()" )
						CASE lcLine = "ENDPROC"
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "		End Sub", "		End Sub" )
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "	#tag EndEvent", "	#tag EndEvent")
						OTHERWISE
							INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, lc2Tabs + loCode.cCode, lc2Tabs + loCode.cCode )
					ENDCASE
				ENDSCAN
				=CloseDBF(lcTempCursor)
			ENDIF
			INSERT INTO (THIS.cFormMethods) VALUES (lcObjName, "#tag EndWindowCode", "#tag EndWindowCode" )
			
			WAIT CLEAR
	ENDCASE
RETURN llRetVal
ENDFUNC


*!* ====================================
*!* Program: ConvertControlEditbox
*!* Author: Kevin Cully
*!* Date: 02/27/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertControlEditbox( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, lcTemplate
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	lcTemplate 		= IIF( THIS.lTargetWeb, "webtextarea", "textarea" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateAnchorProperty( lcTarget, lcProperties )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Begin "+lcTemplate, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			SET STEP ON 
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
		OTHERWISE
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC



*!* ====================================
*!* Program: ConvertControlForm
*!* Author: Kevin Cully
*!* Date: 02/03/12 
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		2/4/2012 - Removed the LEFT and TOP settings because they don't exist at design time in RS2011R4.1
*!*		2/27/2012 - Removed the Height and Width properties from the required fields.  May not exist. 
FUNCTION ConvertControlForm ( toControl, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcProperties 	= [p] + lcBaseFileName
	
	lcTemplate = IIF( THIS.lTargetWeb, "webpage_begin", "window" )
	lcBeginTag = IIF( THIS.lTargetWeb, "Begin WebPage", "Begin Window" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		CASE THIS.PropertiesIntoCursor( toControl.Properties, lcProperties ) < 1
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, lcBeginTag, "Name" )
		CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Title", "Caption" )
		&&CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Left", "Left" )
		&&CASE NOT THIS.UpdateXojoProperty( lcTarget, lcProperties, "Top", "Top" )
		OTHERWISE
			llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Height", "Height" )
			llSuccess = THIS.UpdateXojoProperty( lcTarget, lcProperties, "Width", "Width" )
			
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	=CloseDBF( lcProperties )
	
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program: ConvertControlFormClose
*!* Author: Kevin Cully
*!* Date: 02/03/12
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 3/15/2012 - Had to make llNoSpaces be false because OutputControl was removing spaces from within the values.
FUNCTION ConvertControlFormClose ( toForm, tnLevel )
	LOCAL lcRetVal, lcBaseFileName, lcTarget, lcProperties, llNoSpaces
	lcRetVal = []
	lcBaseFileName 	= SYS( 2015 )
	lcTarget 		= [c] + lcBaseFileName
	lcTemplate		= IIF( THIS.lTargetWeb, "webpage_end", "window_end" )
	
	DO CASE
		CASE NOT THIS.GetControlTemplate( lcTemplate, lcTarget )
			SET STEP ON 
		OTHERWISE
			llNoSpaces = .F.
			lcRetVal = THIS.OutputControl( lcTarget, tnLevel, llNoSpaces )
			&& SET STEP ON 
	ENDCASE
	=CloseDBF( lcTarget )
	
RETURN lcRetVal
ENDFUNC

*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 05/03/11
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertProgram( toProjThing AS Object )
	LOCAL llRetVal, lcProgram, lcProgramCode, llContinue
	llRetVal = .F.
	llContinue = .F.
	lcProgram = toProjThing.Name
	IF NOT FILE( lcProgram )
		lcProgram = ADDBS( ALLTRIM( toProjThing.cHomeDir ) ) + ALLTRIM(toProjThing.Name)
		llContinue = FILE( lcProgram )
	ELSE
		llContinue = .T.
	ENDIF
	
	IF llContinue THEN
		lcProgramCode = FILETOSTR( lcProgram )
		lcProgramCode = THIS.ConvertVFPCodeToXojoCode( lcProgramCode )
		=STRTOFILE( lcProgramCode, "output\" + FORCEEXT( JUSTFNAME( lcProgram ), 'xojo_code'), 0 )

	ELSE
		SET STEP ON 
	ENDIF
	=CloseDBF( THIS.cTempCursor )
	
RETURN llRetVal

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 01/31/12
*!* Copyright:
*!* Description:
*!*		Takes a program file and changes each line into a row in a cursor.
*!* Revision Information:
*!*		KJC 4/28/2012 - Change to move the #TAG lines to lines with the real code.
FUNCTION ProgramIntoCursor ( tcProgramFile AS String, tcTargetCursor AS String )
	LOCAL lnRetVal
	LOCAL nTimeStart, nTimeFinish
	lnRetVal = -1
	
	IF USED(tcTargetCursor)
		SET STEP ON 
	ELSE
		nTimeStart=SECONDS()
		=CloseDBF("c_Temp")
		
		CREATE CURSOR c_Temp ( cCode1 C(200), cCode2 C(200), cCode3 C(200), cCode4 C(200), cCode5 C(200), cBonus C(200) )
		SELECT c_Temp
		APPEND FROM (tcProgramFile) TYPE SDF
		lnRetVal = RECCOUNT("c_Temp")
		
		THIS.ProgramIntoCursorMoveTags( "c_Temp" )
		
		SELECT CAST( cCode1 + cCode2 + cCode3 + cCode4 + cCode5 AS M ) AS cCode, cCode1, cBonus FROM c_Temp WHERE NOT DELETED() INTO CURSOR (tcTargetCursor) READWRITE
		=CloseDBF("c_Temp")
		nTimeFinish=SECONDS()
		&& WAIT WINDOW "Using APPEND FROM SDF to load program: " + TRANSFORM( nTimeFinish - nTimeStart ) + "seconds" TIMEOUT 5
	ENDIF
RETURN lnRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 04/24/12 09:41:14 AM
*!* Copyright:
*!* Description:
*!*		When we are parsing the program, it is much easier when we remove the Xojo #TAG records.
*!* Revision Information:
FUNCTION ProgramIntoCursorMoveTags( tcSourceCursor )
	LOCAL llRetVal, lnOrigArea, lnOrigRec, lc1stWord, lc2ndWord, lnDeletedRec
	llRetVal = .F.
	IF USED( tcSourceCursor )
		lnOrigArea = SELECT()
		lnOrigRec = RECNO()
		SELECT (tcSourceCursor)
		GO TOP
		DO WHILE NOT EOF()
			SCATTER NAME loRec
			lc1stWord = ALLTRIM( UPPER( GETWORDNUM( loRec.cCode1, 1 ) ) )
			IF lc1stWord = "#TAG"
				lnDeletedRec = RECNO()
				lc2ndWord = LEFT( ALLTRIM( UPPER( GETWORDNUM( loRec.cCode1, 2 ) ) ), 3 )
				IF lc2ndWord = "END"
					SKIP -1			&& We need to put this bonus tag in the previous record.
				ELSE
					SKIP 1  		&& We need to put this bonus tag in the next record.
				ENDIF
				REPLACE cBonus WITH loRec.cCode1
				GO (lnDeletedRec)	&& Go back to the orig record so that the SKIP can take place naturally.
				DELETE 				&& Delete this record.
			ENDIF
			SKIP 1
		ENDDO
	ENDIF

RETURN llRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 01/31/12
*!* Copyright:
*!* Description:
*!*		Called from methods including ConvertControl_Methods.
*!* Revision Information:
FUNCTION SnippetIntoCursor( tcSnippet AS String, tcTargetCursor AS String )
	LOCAL lnRetVal, nTimeStart, nTimeFinish, nX, lc1Line
	lnRetVal = -1
	LOCAL ARRAY laArray[1]

	nTimeStart = SECONDS()
	CREATE CURSOR &tcTargetCursor ( cCode M )
	FOR nX = 1 TO ALINES(laArray, tcSnippet)
		lc1Line = laArray[nX]
		INSERT INTO (tcTargetCursor) ( cCode ) VALUES ( lc1Line )
	ENDFOR
	nTimeFinish = SECONDS()
	lnRetVal = RECCOUNT( tcTargetCursor )
RETURN lnRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/15/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION PropertiesIntoCursor( tcSnippet AS String, tcTargetCursor AS String )
	LOCAL lnRetVal, nTimeStart, nTimeFinish, nX, lc1Line, lcProperty, lcValue
	lnRetVal = -1
	LOCAL ARRAY laArray[1]

	nTimeStart = SECONDS()
	CREATE CURSOR &tcTargetCursor ( cProperty C(50), cValue C(200) )
	INDEX ON cProperty TAG cProperty ADDITIVE
	FOR nX = 1 TO ALINES(laArray, tcSnippet)
		lc1Line = laArray[nX]
		lcProperty = ALLTRIM( GETWORDNUM( lc1Line, 1, "=" ) )
		IF NOT EMPTY(lcProperty)
			lcValue = ALLTRIM( GETWORDNUM( lc1Line, 2, "=" ) )
			lcValue = IIF( LEFT( lcValue, 1) = ["], SUBSTR( lcValue, 2 ), lcValue )
			lcValue = IIF( RIGHT( lcValue, 1) = ["], LEFT( lcValue, LEN(lcValue)-1 ), lcValue )
			INSERT INTO (tcTargetCursor) ( cProperty, cValue ) VALUES ( lcProperty, lcValue )
		ENDIF
	ENDFOR
	nTimeFinish = SECONDS()
	lnRetVal = RECCOUNT( tcTargetCursor )
RETURN lnRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 03/13/12
*!* Copyright:
*!* Description:
*!* 	Used to help suck in the Xojo controls to be used in the ControlTemplate table.  The rest of the program
*!*		when run, will use this as a template to spit out valid Xojo text as the export product.
*!* Revision Information:
FUNCTION ControlIntoCursor( tcSnippet AS String, tcTargetCursor AS String )
	LOCAL lnRetVal, nTimeStart, nTimeFinish, nX, lc1Line, lcSetting, lcValue, lcControlType
	lnRetVal = -1
	LOCAL ARRAY laArray[1]

	
	nTimeStart = SECONDS()
	=CloseDBF(tcTargetCursor)
	CREATE CURSOR &tcTargetCursor ( cType C(10), nOrder I, cSetting C(50), cValue C(200), cSettingCl C(10) )
	IF NVL(EMPTY(lcControlType), []) THEN
		lnLines = ALINES( laArray, tcSnippet )
		lc1Line = RTRIM( laArray[1] )
		lcControlType = GETWORDNUM( lc1Line, 2, [ ] )

		FOR nX = 1 TO lnLines
			lcSettingCl = []
			lc1Line = RTRIM( laArray[nX] )
			loProperty = THIS.PropertyToObject( lcControlType, nX, lc1Line )
*!*				DO CASE
*!*					CASE EMPTY(lc1Line)
*!*						&& Do nothing.
*!*					CASE RIGHT(lc1Line,1) = ["]
*!*						lcSetting = GETWORDNUM( lc1Line, 1, ["] ) + ["]
*!*						lcValue = STREXTRACT( lc1Line, ["], ["])
*!*						lcSettingCl = ["]
*!*					CASE [=] $ lc1Line
*!*						lcSetting = LEFT( lc1Line, AT("=", lc1Line) )
*!*						lcValue = GETWORDNUM( lc1Line, 3, [ ] )
*!*					OTHERWISE
*!*						lcValue = GETWORDNUM( ALLTRIM( lc1Line ), 3, [ ] )
*!*						lcSetting = LEFT( lc1Line, LEN(lc1Line)-LEN(lcValue) )
*!*				ENDCASE
			IF NOT EMPTY(loProperty.cSetting)
*!*				INSERT INTO (tcTargetCursor) ( cType, nOrder, cSetting, cValue, cSettingCl) VALUES ( lcControlType, nX, lcSetting, lcValue, lcSettingCl )
				INSERT INTO (tcTargetCursor) FROM NAME loProperty
			ENDIF
		ENDFOR
	ENDIF
	nTimeFinish = SECONDS()
	lnRetVal = RECCOUNT( tcTargetCursor )
RETURN lnRetVal
ENDFUNC

*!* ====================================
*!* Program: PropertyToObject
*!* Author: Kevin Cully
*!* Date: 04/04/12 04:14:11 PM
*!* Copyright:
*!* Description:
*!*		Used to split the Xojo properties in VCP format into a VFP object.
*!* Revision Information:
FUNCTION PropertyToObject( tcType, tnOrder, tc1Line )
	LOCAL loRetVal
	
	IF USED( "tControlTemplate")
		SELECT tControlTemplate
		SCATTER FIELDS EXCEPT ID MEMO BLANK NAME loRetVal
		
		DO CASE
			CASE EMPTY(tc1Line)
				&& Do nothing.
			CASE EMPTY(tcType)
			CASE tnOrder < 1
			OTHERWISE
				loRetVal.cType = tcType
				loRetVal.nOrder = tnOrder
				tc1Line = ALLTRIM( tc1Line )
				
				DO CASE
					CASE RIGHT(tc1Line,1) = ["]
						loRetVal.cSetting = GETWORDNUM( tc1Line, 1, ["] ) + ["]
						loRetVal.cValue = STREXTRACT( tc1Line, ["], ["])
						loRetVal.cSettingCl = ["]
					CASE [=] $ tc1Line
						loRetVal.cSetting = LEFT( tc1Line, AT("=", tc1Line) )
						loRetVal.cValue = GETWORDNUM( tc1Line, 2, [=] )
					OTHERWISE
						loRetVal.cValue = GETWORDNUM( ALLTRIM( tc1Line ), 3, [ ] )
						loRetVal.cSetting = LEFT( tc1Line, LEN(tc1Line)-LEN(loRetVal.cValue) )
				ENDCASE
		ENDCASE
	ENDIF
	
RETURN loRetVal
ENDFUNC


*!*	*!**********************************
*!*	*!* Program:
*!*	*!* Author: CULLY Technologies, LLC
*!*	*!* Date: 01/16/12
*!*	*!* Copyright:
*!*	*!* Description:
*!*	*!*		Takes a cursor of lines of code and then converts them.
*!*	*!*		Returns the new code all assembled together into a string.
*!*	*!* Revision Information:
*!*	FUNCTION ConvertProgramSnippet( tcSourceCursor AS String )
*!*		tcReturnXojo = THIS.ConvertVFPCodeToXojoCode( tcReturnXojo )
*!*		tcReturnXojo = STRTRAN( tcReturnXojo, THIS.cBeginTag, "")
*!*		tcReturnXojo = STRTRAN( tcReturnXojo, THIS.cEndTag, "")
*!*	RETURN tcReturnXojo

*!**********************************
*!* Program: ConvFuncToXojoCode
*!* Author: CULLY Technologies, LLC
*!* Date: 10/02/2013
*!* Copyright:
*!* Description:
*!*		Convert VFP Snippet to 'more like' Xojo snippet
*!* Revision Information:
*!*		KJC 10/8/2013 - Renamed from ConvertFuncToXojoCode to ConvertVFPCodeToXojoCode as we're calling this to convert more than just functions.
FUNCTION ConvertVFPCodeToXojoCode( lcVFPSnippet AS String )
	LOCAL lcXojo
	lcXojo = []

	lnLines = MEMLINES( lcVFPSnippet )

	FOR lnX = 1 TO lnLines
		lcLine = MLINE( lcVFPSnippet, lnX )
		lnWords = GETWORDCOUNT( lcLine )
		lcFirstWord = UPPER( GETWORDNUM( lcLine, 1 ) )
		lcSecondWord = UPPER( GETWORDNUM( lcLine, 2 ) )
		
		&& Global replacements
		lcLine = STRTRAN( lcLine, CHR(38) + CHR(38), [//] )		&& Double ampersand comment
		lcLine = STRTRAN( lcLine, "ALLTRIM(", "TRIM(" )
		lcLine = STRTRAN( lcLine, "alltrim(", "trim(" )
		lcLine = STRTRAN( lcLine, "!THIS.", "NOT ME." )
		lcLine = STRTRAN( lcLine, "!this.", "not me." )
		lcLine = STRTRAN( lcLine, "THIS.", "ME." )
		lcLine = STRTRAN( lcLine, "this.", "me." )
		lcLine = STRTRAN( lcLine, "!THISFORM.", "NOT SELF." )
		lcLine = STRTRAN( lcLine, "!thisform.", "not self." )
		lcLine = STRTRAN( lcLine, "THISFORM.", "SELF." )
		lcLine = STRTRAN( lcLine, "thisform.", "self." )
		lcLine = STRTRAN( lcLine, "MESSAGEBOX(", "MSGBOX(" )
		lcLine = STRTRAN( lcLine, "messagebox(", "msgbox(" )
		lcLine = STRTRAN( lcLine, ".caption", ".text" )
		lcLine = STRTRAN( lcLine, ".value", ".text" )
		
		&& Do we have a line continuation situation where we need to switch from semi-colon to Xojo underscore?
		IF RIGHT( ALLTRIM( lcLine ), 1 ) = ";" THEN
			lcLine = ALLTRIM( lcLine )
			lcLine = LEFT( lcLine, LEN(lcLine)-1 ) + "_" 	&& Change the semi-colon to an underscore
		ENDIF
		
		
		DO CASE
			CASE LEFT( lcFirstWord, 1 ) = [*]
				lcLine = STRTRAN( lcLine, [*], [//*], 1, 1 )

			CASE lcFirstWord = "IF"
				lcLastWord = UPPER( GETWORDNUM( lcLine, lnWords ) )
				IF lcLastWord <> "THEN"
					lcLine = lcLine + " THEN"
				ENDIF

			CASE lcFirstWord = "ENDIF"
				lcLine = STRTRAN( lcLine, "ENDIF", "END IF", 1, 1 )

			CASE lcFirstWord = "ENDFOR"
				lcLine = STRTRAN( lcLine, "ENDFOR", "NEXT", 1, 1 )

			CASE lcFirstWord = "DO" AND lcSecondWord = "CASE"
				lcLine = STRTRAN( lcLine, "DO", "SELECT" ) + [ //<<Insert Variable Here!>>]

			CASE lcFirstWord = "ENDCASE"
				lcLine = STRTRAN( lcLine, "ENDCASE", "END SELECT" )
			
			CASE lcFirstWord = "LOCAL"
				lcLine = STRTRAN( lcLine, "LOCAL", "DIM", 1, 1 ) + [ AS String = ""]

			CASE lcFirstWord = "SET"
				lcLine = STRTRAN( lcLine, "SET STEP ON", "BREAK" )

			CASE lcFirstWord = "FUNCTION "
				TEXT TO lcLine ADDITIVE TEXTMERGE NOSHOW
					#tag Method, Flags = &h0
					<<lcLine>>
				ENDTEXT

			CASE lcFirstWord = "ENDFUNC"
				lcLine = STRTRAN( lcLine, "ENDFUNC", "End Function" ) + THIS.CRLF
				lcLine = lcLine + "#tag EndMethod"

			OTHERWISE

		ENDCASE
		&& If we are commenting out the code, then add a tick at the beginning of the line.
		lcLine = IIF( THIS.lCommentOutCode AND NOT EMPTY( lcLine), "'", "" ) + lcLine
		
		lcXojo = lcXojo + lcLine + CHR(13)
	ENDFOR

RETURN lcXojo

*!**********************************
*!* Program:
*!* Author: CULLY Technologies, LLC
*!* Date: 12/03/11
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION ConvertReport( toProjThing AS Object )
	LOCAL llRetVal
	llRetVal = .F.
RETURN llRetVal

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/15/12
*!* Copyright:
*!* Description:
*!* Revision Information:
*!*		KJC 3/5/12 - Changed the ALLTRIM() to be an RTRIM() from around the loLine.cSetting so we keep 
*!*			the indentation from the settings DBF.
*!*	 	KJC 3/6/2012 - Added the tlNoSpaces parameter, mainly for project output.  Projects don't like spaces.
FUNCTION OutputControl( tcControlCursor, tnLevel, tlNoSpaces )
	LOCAL lcRetVal, loLine, lcSetting, lcSpace
	lcRetVal = []
	lcSpace = IIF( tlNoSpaces, [], [ ] )
	DO CASE
		CASE NOT USED(tcControlCursor)
		OTHERWISE
			SELECT (tcControlCursor)
			SCAN
				SCATTER NAME loLine
				lcSetting = IIF( tlNoSpaces, STRTRAN( loLine.cSetting, SPACE(1), [] ), RTRIM( loLine.cSetting ) )
				lcRetVal = lcRetVal + REPLICATE( CHR(9), tnLevel ) ;
					+ lcSetting ;
					+ IIF( RIGHT( lcSetting, 1 )=["], [], lcSpace ) ;
					+ ALLTRIM(loLine.cValue) ;
					+ ALLTRIM(loLine.cSettingCl) + CHR(10)
			ENDSCAN
	ENDCASE
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program: OutputMethods
*!* Author: Kevin Cully
*!* Date: 02/15/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION OutputMethods( tcMethodsCursor AS String )
	LOCAL lcRetVal, loLine, lcSetting, lcSpace
	lcRetVal = []
	DO CASE
		CASE VARTYPE( tcMethodsCursor ) # "C"
			SET STEP ON 
		CASE EMPTY( tcMethodsCursor )
			SET STEP ON 
		CASE NOT USED(tcMethodsCursor)
			&& SET STEP ON 
		OTHERWISE
			SELECT (tcMethodsCursor)
			SCAN
				SCATTER FIELDS LIKE memoLine NAME loLine MEMO
				lcRetVal = lcRetVal + loLine.memoLine + CHR(10)
			ENDSCAN
	ENDCASE
RETURN lcRetVal
ENDFUNC


*!* ====================================
*!* Program: UpdateXojoProperty
*!* Author: Kevin Cully
*!* Date: 02/15/12
*!* Copyright:
*!* Description:
*!*		Use this to update the values if there is a 1-to-1 translation.
*!* Revision Information:
FUNCTION UpdateXojoProperty( tcXojoPropCursor, tcFoxPropCursor, tcXojoProp, tcFoxProp, tnLocationOffset )
	LOCAL llRetVal, loFoxProp
	llRetVal = .F.
	DO CASE
		CASE NOT USED(tcXojoPropCursor)
		CASE NOT USED(tcFoxPropCursor)
		CASE EMPTY(tcXojoProp)
		CASE EMPTY(tcFoxProp)
		CASE NOT SEEK( tcFoxProp, tcFoxPropCursor )
		OTHERWISE
			SELECT (tcFoxPropCursor)
			SCATTER NAME loFoxProp
			IF NOT ISNULL( tnLocationOffset ) AND VARTYPE( tnLocationOffset ) = "N" AND tnLocationOffset # 0 THEN
				loFoxProp.cValue = TRANSFORM( VAL(loFoxProp.cValue) + tnLocationOffset )
			ENDIF
			
			llRetVal = THIS.UpdateXojoPropertyValue( tcXojoPropCursor, tcXojoProp, loFoxProp.cValue )
	ENDCASE
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/23/12
*!* Copyright:
*!* Description:
*!*		Use this method to update the actual ReadWrite cursor.  
*!* Revision Information:
FUNCTION UpdateXojoPropertyValue( tcXojoPropCursor, tcXojoProp, tcValue, tlContinueIfNotFound )
	LOCAL llRetVal
	llRetVal = .F.

	SELECT (tcXojoPropCursor)
	LOCATE FOR LEFT( ALLTRIM(LOWER( cSetting )), LEN(tcXojoProp) ) == ALLTRIM(LOWER( tcXojoProp ))
	IF FOUND()
		REPLACE cValue WITH tcValue
		llRetVal = .T.
	ELSE
		IF tlContinueIfNotFound
			&& Keep going because this is an optional property.
			llRetVal = .t.
		ELSE
			&& There's a typo in that we should be able to find every tcXojoProp value.
			SET STEP ON 
		ENDIF
	ENDIF

RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/24/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION UpdateAnchorProperty( tcXojoPropCursor, tcVFPPropCursor )
	LOCAL llRetVal, LockLeft, LockRight, LockTop, LockBottom, lcTrue, lcValue, lnAnchor
	llRetVal = .F.
	STORE [""] TO LockLeft, LockRight, LockTop, LockBottom
	lcTrue = [True]

	DO CASE
		CASE NOT USED( tcXojoPropCursor )
			SET STEP ON 
		CASE NOT USED( tcVFPPropCursor )
			SET STEP ON 
		CASE NOT SEEK( "Anchor", tcVFPPropCursor )
			&& We don't have an anchor property.
			llRetVal = .T.
		OTHERWISE
			SELECT (tcVFPPropCursor)
			SCATTER FIELDS LIKE cValue MEMVAR
			lnAnchor = INT( VAL( m.cValue ) )
			IF lnAnchor > 0
				IF lnAnchor >= 512			&& Vertical Fixed
					LockBottom = lcTrue
					LockTop = lcTrue
					lnAnchor = lnAnchor - 512
				ENDIF
				IF lnAnchor >= 256			&& Horizontal Fixed
					LockRight = lcTrue
					LockLeft = lcTrue
					lnAnchor = lnAnchor - 256
				ENDIF
				IF lnAnchor >= 128			&& Right Relative
					LockRight = lcTrue
					lnAnchor = lnAnchor - 128
				ENDIF
				IF lnAnchor >= 64			&& Bottom Relative
					LockBottom = lcTrue
					lnAnchor = lnAnchor - 64
				ENDIF
				IF lnAnchor >= 32			&& Left Relative
					LockLeft = lcTrue
					lnAnchor = lnAnchor - 32
				ENDIF
				IF lnAnchor >= 16			&& Top Relative
					LockTop = lcTrue
					lnAnchor = lnAnchor - 16
				ENDIF
				IF lnAnchor >= 8			&& Right Absolute
					LockRight =  lcTrue
					lnAnchor = lnAnchor - 8
				ENDIF
				IF lnAnchor >= 4			&& Bottom Absolute
					LockBottom = lcTrue
					lnAnchor = lnAnchor - 4
				ENDIF
				IF lnAnchor >= 2			&& Left Absolute
					LockLeft = lcTrue
					lnAnchor = lnAnchor - 2
				ENDIF
				IF lnAnchor >= 1
					LockTop = lcTrue
					lnAnchor = lnAnchor - 1
				ENDIF
				llRetVal = THIS.UpdateXojoPropertyValue( tcXojoPropCursor, "LockLeft", LockLeft )
				llRetVal = llRetVal = THIS.UpdateXojoPropertyValue( tcXojoPropCursor, "LockRight", LockRight )
				llRetVal = llRetVal = THIS.UpdateXojoPropertyValue( tcXojoPropCursor, "LockBottom", LockBottom )
				llRetVal = llRetVal = THIS.UpdateXojoPropertyValue( tcXojoPropCursor, "LockTop", LockTop )
			ENDIF
	ENDCASE	
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 03/05/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION UpdateBackStyleProperty( tcXojoPropCursor, tcVFPPropCursor )
	LOCAL llRetVal, lcTrue, lcValue, lnBackStyle
	llRetVal = .F.
	lcTrue = [True]

	DO CASE
		CASE NOT USED( tcXojoPropCursor )
			SET STEP ON 
		CASE NOT USED( tcVFPPropCursor )
			SET STEP ON 
		CASE NOT SEEK( "BackStyle", tcVFPPropCursor )
			&& We don't have an backstyle property.
			llRetVal = .T.
		OTHERWISE
			SELECT (tcVFPPropCursor)
			SCATTER FIELDS LIKE cValue MEMVAR
			lnBackStyle = INT( VAL( m.cValue ) )
			IF NOT THIS.lTargetWeb AND lnBackStyle = 0 THEN		&& Zero means Transparent
				llRetVal = THIS.UpdateXojoPropertyValue( tcXojoPropCursor, "Transparent", lcTrue )
				llRetVal = .T.
			ENDIF
	ENDCASE	
RETURN llRetVal
ENDFUNC


*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/03/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION GetPropertySetting( tcProperties, tcSetting )
	LOCAL lcRetVal
	lcRetVal = []
	DO CASE
		CASE VARTYPE( tcProperties ) # "C"
		CASE VARTYPE( tcSetting ) # "C"
		CASE EMPTY( tcProperties )
		CASE EMPTY( tcSetting )
		OTHERWISE
			lcAlias = "c" + SYS(2015)
			lnPropertyCount = THIS.SnippetIntoCursor( tcProperties, lcAlias )
			IF lnPropertyCount > 0
				LOCATE FOR LEFT(cCode, LEN(tcSetting)) = tcSetting
				IF FOUND()
					lcRetVal = ALLTRIM( GETWORDNUM( cCode, 2, "=" ) )
				ELSE
					SET STEP ON 
				ENDIF
			ENDIF
			=CloseDBF( lcAlias )
	ENDCASE
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 03/06/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION GetVFPValue( tcProperties, tcSetting )
	LOCAL lcRetVal, lnOrigArea
	lcRetVal = []
	DO CASE
		CASE VARTYPE( tcProperties ) # "C"
		CASE VARTYPE( tcSetting ) # "C"
		CASE EMPTY( tcProperties )
		CASE EMPTY( tcSetting )
		CASE NOT USED( tcProperties )
		CASE NOT SEEK( tcSetting, tcProperties )
			SET STEP ON 
		OTHERWISE
			lnOrigArea = SELECT()
			SELECT (tcProperties)
			lcRetVal = cValue
			
			SELECT (lnOrigArea)
	ENDCASE
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/13/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION GetControlTemplate( tcType, tcTarget )
	LOCAL llRetVal
	llRetVal = .F.
	
	=OpenDBF("data\tControlTemplate")
	
	DO CASE
		CASE ISNULL( tcType )
		CASE ISNULL( tcTarget )
		CASE VARTYPE( tcType ) # "C"
		CASE VARTYPE( tcTarget ) #"C"
		CASE EMPTY( tcType )
		CASE EMPTY( tcTarget )
		CASE NOT USED("tControlTemplate")
			SET STEP ON 
		OTHERWISE
			SELECT * FROM tControlTemplate ;
				WHERE ALLTRIM(LOWER(cType))== ALLTRIM(LOWER(tcType)) ;
				ORDER BY nOrder INTO CURSOR &tcTarget READWRITE
			llRetVal = ( _TALLY > 0 )
	ENDCASE
	
RETURN llRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 02/03/12
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION GetControlUniqueID( tcObjName )
	LOCAL lcRetVal
	lcRetVal = []
	DO CASE
		CASE VARTYPE( tcObjName ) # "C"
		CASE EMPTY( tcObjName )
		CASE NOT USED( THIS.cTempCursor )
		OTHERWISE
			lnOrigArea = SELECT()
			SELECT (THIS.cTempCursor)
			LOCATE FOR ObjName = tcObjName
			IF FOUND()
				lcRetVal = UniqueID
			ENDIF
			SELECT (lnOrigArea)
	ENDCASE
RETURN lcRetVal
ENDFUNC

*!* ====================================
*!* Program:
*!* Author: Kevin Cully
*!* Date: 03/13/12 10:41:08 AM
*!* Copyright:
*!* Description:
*!* Revision Information:
FUNCTION CreateMethodsCursor( tcTargetCursor AS String )
	LOCAL llRetVal
	llRetVal = .F.
	DO CASE
		CASE ISNULL( tcTargetCursor )
			SET STEP ON 
		CASE VARTYPE( tcTargetCursor ) # "C"
			SET STEP ON 
		CASE EMPTY( tcTargetCursor )
			SET STEP ON 
		CASE USED( tcTargetCursor )
			llRetVal = .T.
		OTHERWISE
			lcCommand = [CREATE CURSOR ] + tcTargetCursor + ;
				[(cControlName C(50), cLine C(200), memoLine M)]
			&lcCommand
			llRetVal = USED( tcTargetCursor )
	ENDCASE
RETURN llRetVal
ENDFUNC

*!************************************************
*!* Program: CleanString
*!* Author: CULLY Technologies, LLC
*!* Date: 02/26/07 02:34:07 PM
*!* Copyright:
*!* Description: Sometimes, strings need to be cleaned of non valid chars.
*!*		The SearchType is what is ALLOWED in the string.
*!* Revision Information:
FUNCTION CleanString(tcString AS String, tcSearchType AS String)
	LOCAL lcRetVal, lcSearchString
	lcRetVal = []
	lcSearchString = []
	
	IF "A" $ tcSearchType				&& Alpha characters
		lcSearchString = lcSearchString + [ABCDEFGHIJKLMNOPQRSTUVWXYZ]
	ENDIF
	IF "C" $ tcSearchType				&& Comma
		lcSearchString = lcSearchString + [,]
	ENDIF
	IF "E" $ tcSearchType				&& Email punctuation.  Be sure to include "A" and "N" yourself.
		lcSearchString = lcSearchString + [@.-_]
	ENDIF
	IF "F" $ tcSearchType				&& File punctuation
		lcSearchString = lcSearchString + [-_.]
	ENDIF
	IF "N" $ tcSearchType				&& Numbers
		lcSearchString = lcSearchString + [0123456789]
	ENDIF
	IF "P" $ tcSearchType				&& Punctuation
		lcSearchString = lcSearchString + [.,;!@#$%^&*()<>?/\|{}-_=]
	ENDIF
	IF "S" $ tcSearchType				&& Space
		lcSearchString = lcSearchString + [ ]
	ENDIF
	IF "_" $ tcSearchType				&& Underscore
		lcSearchString = lcSearchString + [_]
	ENDIF
	IF "-" $ tcSearchType				&& Dash
		lcSearchString = lcSearchString + [-]
	ENDIF
	IF "." $ tcSearchType				&& Period
		lcSearchString = lcSearchString + [.]
	ENDIF
	IF ["] $ tcSearchType				&& Double Quote
		lcSearchString = lcSearchString + ["]
	ENDIF
	IF [@] $ tcSearchType				&& 'At' sign.
		lcSearchString = lcSearchString + [@]
	ENDIF
	lcSearchString = lcSearchString + IIF(['] $ tcSearchType, ['], [])  	&& Single Quote

	FOR lnX = 1 TO LEN(tcString)
		lcChar = SUBSTR(tcString, lnX, 1)
		IF AT(UPPER(lcChar), lcSearchString) > 0
			lcRetVal = lcRetVal + lcChar
		ENDIF
	ENDFOR
	
    RETURN lcRetVal
ENDFUNC



*=========================================================
*=========================================================
ENDDEFINE
*=========================================================
*=========================================================
*=========================================================


*////////////////////////////////////////////////////////////////
FUNCTION OpenDbf
	PARAMETER tcTable, tcOrder, tcAlias, plExclusive
	PRIVATE lcOrder, lcAlias, lcAliasCmd, lcExclusive

	llRetVal = .F.
	tcOrder = EnsureChar(tcOrder)

	IF EMPTY(tcAlias)
		lcAlias = JUSTSTEM(tcTable)
	ELSE
		lcAlias = tcAlias
	ENDIF

	*** If the table includes a path and that path has a space in it, we need the quotes around it. - KJC 11/29/2001
	IF ATC([ ], tcTable) > 0
		tcTable = ['] + tcTable + [']
	ENDIF

	IF NOT USED(lcAlias)
		lcAliasCmd = 'ALIAS ' + lcAlias
		lcExclusive = "SHARED"
		IF TYPE('plExclusive') = 'L'
			lcExclusive = IIF(plExclusive, "EXCLUSIVE", "SHARED")
		ENDIF
		USE &tcTable IN 0 &lcAliasCmd &lcExclusive AGAIN
	ENDIF

	IF USED(lcAlias)
		SELECT &lcAlias
		IF NOT EMPTY(tcOrder)
			SET ORDER TO &tcOrder
		ENDIF
		llRetVal = .T.
	ENDIF
	RETURN llRetVal

*////////////////////////////////////////////////////////////////
FUNCTION CloseDBF
	PARAMETER tcAlias
	PRIVATE llRetVal
	llRetVal = .F.
	IF USED(tcAlias)
		SELECT &tcAlias
		USE
		llRetVal = .T.
	ENDIF
	RETURN llRetVal

*////////////////////////////////////////////////////////////////
FUNCTION EnsureChar
LPARAMETER tuVariable
	LOCAL lcRetVal
	lcRetVal = []
	IF VARTYPE(tuVariable) = [C]
		lcRetVal = tuVariable
	ENDIF
RETURN lcRetVal
ENDFUNC

